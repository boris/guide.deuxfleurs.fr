---
title: "Statique ? Comment ça ?"
description: "Clarification sur le contenu statique"
date: 2022-09-01
weight: 1
extra:
  parent: "prise_en_main/web.md"
---

Au commencement du web (au début des années 90), le contenu présent en ligne était statique : quand un client veut un fichier ou une page, il spécifie le chemin via un URL, et le serveur répond tout le temps la même chose. Dès le milieu des années 90 ont apparu au sein du web des pages dynamiques, avec [CGI](https://fr.wikipedia.org/wiki/Common_Gateway_Interface) et [PHP](https://fr.wikipedia.org/wiki/PHP). Pour une même page, le serveur peut répondre un contenu différent, variant selon des paramètres rajoutés en fin de chemin URL, selon des données dans la mémoire du serveur, ou selon n'importe quel facteur qui passe par la tête du développeur.

Avec Javascript, qui date de la même époque, une confusion se pose souvent : beaucoup de contenus statiques passent pour du dynamique aux yeux de l'internaute. Il faut alors rappeler une distinction importante : le code Javascript livré avec un site s'exécute côté client, c'est-à-dire sur l'ordinateur ou le téléphone de l'internaute. À contrario, le CGI ou PHP s'exécute côté serveur, c'est-à-dire sur la machine administrée par le propriétaire du site. Selon ce qu'on veut faire, il faut choisir l'un ou l'autre.

Quelques exemples pour comprendre cette dichotomie : je gère une boutique en ligne, et je veux que le stock de mes produits s'affiche quand un client consulte mon site ? Il faut faire ça côté serveur, car seul lui connaît les stocks. Je veux mettre en place des décors interactifs avec la souris ? Il faut faire ça côté client, car seul lui connaît la position du curseur. Là où le bât blesse, c'est que certaines choses peuvent être faites des deux côtés, par exemple afficher l'heure sur une page.

En conclusion, une page statique peut proposer des interactions et des effets quelconques, et même se comporter comme une application, car l'interactivité est gérée uniquement par votre ordinateur. Dans ce modèle, par contre, le serveur web se contente simplement de vous envoyer les données que vous lui demandez, sans exécuter de logique complexe.
