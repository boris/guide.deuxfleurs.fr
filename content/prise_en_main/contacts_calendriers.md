---
title: "Contacts & Calendriers"
description: "Contacts & Calendriers"
weight: 21
extra:
  parent: 'prise_en_main/_index.md'
---

Vous pouvez utiliser Deuxfleurs pour synchroniser vos contacts et vos calendriers ! Par exemple, pour retrouver sur votre téléphone les adresses et rendez-vous que vous avez prévu sur le client web de votre ordinateur.

## Avec notre web-client e-mail SOGo

Sur [https://sogo.deuxfleurs.fr/](https://sogo.deuxfleurs.fr/), vous pouvez accéder à vos e-mails, mais aussi à un onglet Calendrier et à un onglet Contacts. 
Ce sont ces deux listes qu'on peut synchroniser avec votre téléphone.

## Sur téléphone Android

Pour que votre téléphone discute avec notre infrastructure, on recommande d'installer l'application libre [DAVx5](https://www.davx5.com/). 
Elle est disponible sur le Play Store, ou mieux, sur [F-Droid](https://f-droid.org/) (un magasin d'applications libres).

Une fois l'application installée, il faut ajouter un compte, choisir l'option `Connexion avec une URL et un nom d'utilisateur`, et rentrer : 

* `Url de base` : `https://sogo.deuxfleurs.fr/SOGo/dav/`
* `Nom d'utilisateur` : votre adresse e-mail
* `Mot de passe` : votre mot de passe Deuxfleurs

Validez en cliquant sur `Se connecter`. Vous devriez voir apparaître un encart avec votre adresse Deuxfleurs. En cliquant dessus, vous aurez accès à plusieurs onglets, notamment Calendriers et Contacts, sur lequel vous devriez voir une ou plusieurs lignes. Cochez les lignes que vous voulez synchroniser avec votre téléphone.

### Contacts 

Le reste se passe dans votre application de contacts, où vous pouvez sélectionner diverses listes de contact à afficher. 

Désormais, si vous voulez ajouter un contact, vous pourrez sélectionner votre liste de contacts Deuxfleurs lorsqu'on vous demandera à quelle liste l'ajouter (SIM, téléphone, compte Google...).

### Calendrier

Similairement, c'est dans votre application d'agenda que vous allez pouvoir afficher le calendrier Deuxfleurs et y ajouter des événements.

## Sur ordinateur

### Calendrier

Sur le client e-mail **Mozilla Thunderbird**, pour accéder en lecture & écriture à votre agenda, vous devez : 

* vous rendre dans l'onglet Agenda, cliquer sur « Nouvel agenda », et sélectionner « Sur le réseau ». On demande alors deux informations :

  * `Nom d'utilisateur` : renseignez votre adresse e-mail complète,
  * `Adresse` : vous l'obtiendrez en vous connectant à [SoGO](https://sogo.deuxfleurs.fr), onglet « Agenda », cliquez sur les 3 points verticaux à droite de votre agenda dans le menu de gauche, puis « Liens vers cet agenda ». 

    Prenez le premier lien : « Accès aux utilisateurs authentifiés > Accès en CalDAV ». 

    Chez moi, cela donne (remplacez bien `MON_PSEUDO` par votre nom d'utilisateur !) :

        http://sogo.deuxfleurs.fr/SOGo/dav/MON_PSEUDO/Calendar/personal/

* De retour dans Thunderbird, on peut cliquer sur « Rechercher des agendas ».
* On vous demande alors votre mot de passe (celui de votre compte Deuxfleurs).
* Vous devriez alors voir votre agenda apparaître. Il ne reste qu'à cliquer sur « S'abonner » !


