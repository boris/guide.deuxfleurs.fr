---
title: "À la main"
description: "Créer du contenu à la main"
sort_by: "weight"
date: 2022-09-01
weight: 1
extra:
  parent: "prise_en_main/creer-du-contenu.md"
---

Garage, comme tout serveur web, va se contenter ici de servir des fichiers `.html`. Ces fichiers sont tout à fait lisibles et écrivables par un humain. Ainsi, si vous n'avez pas prévu d'avoir un site avec beaucoup de pages, il est souvent intéressant de façonner chacune d'entre elles à la main.

### Décrire le contenu

Pour écrire un page web, il faut écrire un fichier `.html` en suivant, justement, la syntaxe et les règles HTML. À titre indicatif, voici un exemple simple :
```
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Mon site</title>
  </head>
  <body>
    <p>Bonjour; bienvenue sur mon site !</p>
  </body>
</html>
```
Vous pourrez facilement trouver moult ressources en ligne pour maîtriser le HTML. [La documentation de Mozilla sur le sujet](https://developer.mozilla.org/fr/docs/Web/HTML) est un bon départ. Enfin, il faut savoir que le HTML est un langage pour décrire le contenu de la page seulement, pas l'allure ou l'esthétique ! La conséquence directe est qu'une page reposant uniquement sur HTML sera sobre : texte noir sur fond blanc, avec alignement à gauche. Si cela vous suffit, vous pouvez d'ores et déjà vous arrêter sur ça.

### Décrire l'apparence
Si vous souhaitez ajouter des couleurs, modifier la disposition, ou l'arrangement par exemple, il faut rajouter, au-dessus du HTML, une description CSS qui contient vos règles esthétiques. Vous pouvez par exemple créer un fichier `style.css` à la racine du dossier représentant votre site. Ensuite, dans vos pages HTML que vous souhaitez styliser, il faut rajouter, dans la section `<head>`, la formule suivante : `<link rel="stylesheet" type="text/css" href="style.css">` (attention le chemin de `style.css` est relatif, par exemple si vous voulez styliser une page dans un dossier, il faudrait alors marquer `../style.css` à la place). Une fois ceci fait, lorsque quelqu'un va visiter une page HTML, il va automatiquement récupérer le fichier CSS associé, et l'appliquer. Voici encore une fois, à titre indicatif, un contenu exemple pour `style.css` :
```
body
{
  margin: 0 auto;
  max-width: 1000px;
  padding: 10px 10px 10px 10px;
}
```
Encore une fois, plein de ressources sont disponibles en ligne, et Mozilla propose encore une fois [une bonne base](https://developer.mozilla.org/fr/docs/Web/CSS).

Une fois que vous avez tous vos fichiers `.html` et `.css` réunis, [vous pouvez passer à la publication](@/prise_en_main/publier-le-contenu.md) !
