---
title: "Sous Linux (avancé)"
description: "Drive Garage avec Rclone (guide avancé)"
weight: 10
extra:
  parent: 'prise_en_main/stockage.md'
---

**Public visé:** des utilisateurs sous Linux n'ayant pas peur de la ligne de commande.

**Objectif:**

- stocker vos données chez Deuxfleurs et les faire apparaître dans votre navigateur de fichier comme un disque réseau, ce qui vous permet d'y accéder comme à des fichiers normaux.
- optionnellement, ajouter une couche de chiffrement afin que vos fichiers ne soient pas stockés en clair sur les serveurs de Deuxfleurs.

**Attention:** ce tutoriel vous permet d'accéder à un dossier partagé depuis plusieurs machines. Cependant, Garage n'est pas capable de gérer l'édition en simultané d'un document depuis plusieurs lieux différents. Avec la méthode présentée ici, **ne modifiez jamais un fichier stocké sur Garage depuis plusieurs ordinateurs en même temps: vous pourriez perdre des données!**

**En cas de problème avec ce tutoriel:** rapporter votre souci sur le chan Matrix `#tech:deuxfleurs.fr` ou directement par mail à `alex [at] adnab.me`.

## 1. Obtenir des clefs d'accès Garage

La première étape consiste à obtenir vos clefs d'accès Garage. Pour l'instant la procédure n'est pas encore automatisée: vous devez demander à un administrateur qu'il vous envoie ces clefs manuellement. Demandez-lui également de créer un "bucket" pour vos fichiers personnels: pour l'instant vous ne pouvez pas encore le faire vous-même.

Vous devriez obtenir une paire de clefs de la forme suivante:

- L'identifiant de votre clé d'accès (exemple : `GKae0f36c...`), également connu sous le nom de "Access key" ou "Access key ID"
- Votre clé d'accès secrète (exemple : `ae6df87221edd...`), également connue sous le nom de "Secret key" ou "Secret access key"

## 2. Configurer `rclone`

Avant de suive ce guide, assurez-vous que la commande `rclone` est installée sur votre système Linux.

La première étape consiste à configurer `rclone` pour accéder à vos dossier stockés sur Garage. Cette configuration est suffisante dans un premier temps si vous ne souhaitez pas chiffrer vos fichiers sur le serveur. La section suivante est optionnelle et permettera d'introduire la couche de chiffrement.

### 2.1. Configurer le backend Garage

Créez le fichier `~/.config/rclone/rclone.conf` si celui-ci n'existe pas. Ensuite, modifiez ce fichier pour y ajouter la section suivante:

```ini
[grgdf]
type = s3
provider = Other
env_auth = false
access_key_id = GKae0f36c...
secret_access_key = ae6df87221edd...
endpoint = https://garage.deuxfleurs.fr
region = garage
```

Précisez les bonnes valeurs de `access_key_id` et `secret_access_key` que vous avez obtenu dans la phase 1.

Ensuite, testez votre configuration en faisant `rclone lsd grgdf:`. Vous devriez avoir une sortie ressemblant à ceci:

```
$ rclone lsd grgdf:
          -1 2021-11-24 17:10:05        -1 alex.perso
$
```

Ce résultat indique que vous disposez d'un bucket (un dossier de premier niveau) sur le stockage, ici appelé `alex.perso`. Celui-ci a été créé par un administrateur et vous ne pouvez pas en créer d'autre vous-même. Retenez son nom pour l'étape suivante.

### 2.2. Configurer la couche de chiffrement `rclone`

Pour cette étape, nous allons utiliser l'assistant de configuration interactif de `rclone`. Si vous ne souhaitez pas utiliser le chiffrement, cette étape est optionnelle, mais elle est en réalité simple à réaliser donc je vous conseille de le faire.

**Prenez garde à copier vos mots de passez de chiffrement à plusieurs endroits : si vous les perdez, vos données deviendront définitivement illisibles !**

Pour lancer l'assistant interactif, tapez `rclone config` dans un terminal. Ensuite, entrez les valeurs suivantes:

- Tapez `n` pour `New remote`
- Dans `name`, entrez le nom `grgdfcrypt`
- Comme type de stockage, choisissez `Encrypt or décrypt a remote` (option `12`)
- Comme remote, entrez `grgdf:` suivi du nom du bucket qui vous a été donné, par exemple: `grgdf:alex.perso`.
- Choisissez l'option `encrypt the file names` (option `1` ou mode `standard`)
- Choisissez l'option `encrypt directory names` (option `1`)
- Pour le premier mot de passe, tapez `g` pour `Generate password`.
- Entrez 128 pour la taille du mot de passe.
- Prenez note du mot de passe affiché et gardez-le en lieu sûr pour être sûr de pouvoir déchiffrer vos données dans le future. Confirmez en tapant `y`
- On vous demande ensuite si vous voulez utiliser un sel personnalisé. Tapez `g` pour en créer un automatiquement.
- Entrez 128 pour la taille du sel.
- Prenez note également du sel, qui fonctionne comme un second mot de passe. Confirmez en tapant `y`
- Tapez `n` à la question `edit advanced config?`
- Vérifiez la configuration proposée, et validez en tapant `y`

Vous pouvez vérifier que votre configuration a bien réussi en affichant le contenu du fichier `~/.config/rclone/rclone.conf`. Vous devriez y voir une section comme la suivante:

```ini
[grgdfcrypt]
type = crypt
remote = grgdf:alex.perso
password = <...>
password2 = <...>
```

Pour tester que votre dossier chiffré est bien configuré, vous pouvez par exemple:

- taper `rclone ls grgdfcrypt:` pour lister tous les fichiers présents (cette commande renvoie initalement un résultat vide car il n'y a aucun fichier)
- copier un fichier local vers le dossier chiffré en tapant `rclone copy mon/fichier/local.txt grgdfcrypt:monfichier.txt`, ce fichier devrait ensuite apparaître en tapant `rclone ls grgdfcrypt:`

## 3. Configurer un service `systemd` utilisateur

Nous allons maintenant faire en sorte que vos dossiers Garage apparaissent comme des disques réseau montés sur votre système de fichier, ce qui vous permettera d'y accéder depuis votre explorateur de fichier comme s'il s'agissait de dossiers normaux.

Pour commencer, choisissez un endroit où monter votre disque réseau. Dans cet exemple, j'utiliserai `$HOME/mnt/grgdf` pour le dossier non chiffré, et `$HOME/mnt/grgdfcrypt` pour le dossier chiffré. Créez ces deux dossier vides:

```bash
mkdir -p $HOME/mnt/grgdf
mkdir -p $HOME/mnt/grgdfcrypt
```

Créez ensuite le fichier `~/.config/systemd/user/rclone-mount@.service` et copiez-y le contenu suivant:

```ini
[Unit]
Description=Mount a directory using rclone
After=network.target

[Service]
ExecStartPre=sh -c "fusermount -u ${HOME}/mnt/%i || exit 0"
ExecStart=/usr/bin/rclone mount --vfs-cache-mode minimal --cache-dir ${HOME}/.cache/rclone/%i %i: ${HOME}/mnt/%i

RestartSec=10
Restart=always

[Install]
WantedBy=default.target
```

Vous pouvez ensuite monter ("activer") vos dossiers Garage avec l'une des commandes suivantes:

- `systemctl --user start rclone-mount@grgdf` pour le dossier non chiffré
- `systemctl --user start rclone-mount@grgdfcrypt` pour le dossier chiffré

Pour que ceux-ci soient montés automatiquement en permanence, tapez:

- `systemctl --user enable rclone-mount@grgdf` pour le dossier non chiffré
- `systemctl --user enable rclone-mount@grgdfcrypt` pour le dossier chiffré

Pour tester le fonctionnement de votre dossier Garage, vous pouvez naviguer dans ce dossier avec votre explorateur de fichier, tenter d'y copier des fichiers, etc.

## 4. Configurer une seconde machine pour accéder au dossier

Vous pouvez assez facilement configurer une seconde machine pour accéder au même dossier partagé sur Garage. Pour cela, reprenez le fichier `~/.config/rclone/rclone.conf` de votre première machine et copiez-le au même endroit sur votre seconde machine. Vous pouvez ensuite réaliser sur votre deuxième machine les étapes mentionnées dans la section 3 pour le service systemd.

**ATTENTION:** Garage ne supporte pas l'édition simultanée d'un document depuis plusieurs machines. Si vous ouvrez un fichier pour le modifier en même temps sur des machines différentes, **vous prenez le risque de perdre des données**. Pour utiliser votre dossier Garage comme un lieu de travail collaboratif, assurez-vous que des personnes différentes devant modifier un même fichier prendront chacun leur tour bien défini pour le faire.

## Références

- <https://rclone.org/crypt/>
- <https://adnab.me/cgit/user-config.git/tree/lindy/systemd/rclone-mount@.service>
