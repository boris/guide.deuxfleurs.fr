---
title: "Évènements"
description: "Compte-rendu des évènements auxquel on a assisté"
weight: 30
sort_by: "weight"
extra:
  parent: 'formations/_index.md'
---

Compilation des comptes-rendus des évènements auxquels Deuxfleurs a participé ou même organisé.
L'idée c'est de créer une base de connaissance partagée pour que ce qui se soit dit ne soit pas perdu,
puisse être partagé et réutilisé.

# 2023

[Privemère 2023](@/formations/primevère-2023.md)

[JDLL 2023](@/formations/jdll-2023.md)

*Pas Sage En Seine*

*Maison de l'environnement - (Dé)connexion*

[Grand RDV Vénissieux - Numérique Durable](@/formations/rdv-2023.md)

# 2022


[Capitole du libre, fin 2022](@/formations/capitole-du-libre-2022.md)

[Sobriété Numérique au Soly, juin 2022](@/formations/capitalisme_surveillance.md) - Interroger la notion de sobriété numérique sous l'angle de la critique du capitalisme de surveillance.

[(Camps CHATONS) Le libre en Europe](@/formations/libre-europe.md) - Comment financer ses déplacements à des conférences, des hackmeeting ou ses rencontres de consoeurs/frères europeen avec ERASMUS mobilité éducation des adultes. Découvrir le collectif Librehosters. Présentation du projet OpenMinds porté par MarsNet

[(Camps CHATONS) Sauvegardes coopératives](@/formations/sauvegardes-cooperatives.md) - Projet de création de plateformes de sauvegardes coopératives et standardisées au sein des CHATONS.

[(Camps CHATONS) Éducation populaire](@/formations/education-populaire.md) - Se réunir pour parler d'éducation car ce terme apparaît sur le site Chatons.org, il est revendiqué mais pourtant il n'est pas perçu de la même manière par tou·te·s ! Comment se l'approprier ?

[(Camps CHATONS) Écologie](@/formations/ecologie.md) - Quel discours écologique tenir en tant qu'hébergeur ? Comment ne pas tomber dans le greenwashing ? quel arbitrage avec d'autres enjeux ? Durant cet échange, on a essayé de référencer des pistes pour agir collectivement mais aussi de recenser nos démarches déjà existantes et voir à les documenter mieux.

[(Camps CHATONS) Émancip'Asso](@/formations/emancipasso.md) - Des assos aimeraient transitionner vers le libre mais souvent la transition est moyennement heureuse. L'idée serait de faciliter cette transition en travaillant avcec les CHATONS sur la com, la formation, l'accompagnement, etc.

[(Camps CHATONS) Compilation PDF de tous les ateliers du camps](/img/2022-libreto-camps-chatons.pdf)  
[(Camps CHATONS) Libreto camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022)
