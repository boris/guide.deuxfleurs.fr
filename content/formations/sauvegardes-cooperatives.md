---
title: "Sauvegardes coopératives"
description: "Sauvegardes coopératives entre CHATONS"
weight: 10
extra:
  parent: 'formations/conf.md'
---

**Animateurs :** Équipe Picasoft  
**Prise de note, compte-rendu :** Quentin de Deuxfleurs  
**Document source :** [Libreto camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022/samedi+20+-+ateliers)

Vers des sauvegardes solidaires et résilientes.

# À quoi ça sert les sauvegardes ?

Parce qu'il peut y avoir des incidents.
Exemple de OVH avec une défaillance matérielle, exemple d'une erreur humaine
Garder un historique des données.
Restaurer les données telle qu'elles étaient il y a une semaine.
On a aussi envie de répartir les sauvegardes à différents endroits.
État des lieux chez les CHATONS
Aimerait faire un sondage sur comment les CHATONS gèrent leurs sauvegardes.

## Partage d'expérience

Retzien Libre -> Pas toutes les données sauvegardées de la meme maniere.
Séparer les données crées par les users car irrecuperables si perdues, alors que les données systèmes sont reconfigurables.
Chez le Retzien, dump de la machine virtuelle.
Par contre les données Nextcloud plus sensibles, sauvegardes quotidiennes pour pouvoir les récupérer plus rapidement.

Doubler ces sauvegardes par des sauvegardes distantes.
Deux serveurs, deux sites, les sauvegardes de la veille sont envoyées sur l'autre site.
Sauvegade J-0 en local, sauvegarde J-1 à distance.
3 sauvegardes donc.

Distrilab sur Proxmox aussi.
Sauvegarde locale une par jour, sur le meme serveur.
Replication sur l'autre serveur tous les 1/4 heures.
Pour distance, cron pour copier les données à distance sur un NAS.
0 historique. Peu faire de l'historique avec Proxmox mais prend vite de la place car pas de déduplication.
Recemment ont essayé Proxmox Backup server, pas completement content car ça prend bcp de temps.

Exarius : volume BTRFS, snapshot, backup disque externe
Deuxfleurs : restic sur du minio pour le SQL. À plat sur sur du BTRFS pour Garage
42l : borg chez Picasoft, append only, chiffrement, réflexion sur la sécu.

## En pratique

**Méthode :** Gradation dans la complexité/efficacité : à la main, Sauvegarde auto, Sauvegarde auto + rotation automatique, Tests automatiques et autres propriétés avancées

**Stockage :** Même gradation : meme disque, meme ordi, support amovible, location en datacenter, cloud ?!

**Problème :** stocker à distance des backups ça coute potentiellement cher.

# Imaginer une collaboration entre les CHATONS pour les sauvegardes

Constat que c'est un sujet commun, complexe, avec une composante humaine - le risque d'erreur existe, il y a une responsabilité importante en cas de perte de données, etc. Face à ce constat, l'attrait du cloud est fort pour nombre d'entre nous avec sa promesse d'externalisation des risques. L'idée c'est de réfléchir comment on pourrait être autonome sur ce sujet.

## Proposition 1 - partageons nos sauvegardes

Chacun-e va voir d'autres CHATONS pour demander de l'espace de stockage.
Difficultés
 - multiplication des interlocuteurices
 - heterogeneité des acces (ssh, ftp, etc.)
 - vérifier que tout fonctionne

Gérer cette complexité est trop compliqué.
Besoin de normaliser les outils de sauvegarde.

## Recherches sur la normalisation

Il y a plein de façons différentes de faire des sauvegardes, via SSH par exemple, etc.
La proposition de Picasoft c'est d'utiliser le protocole S3 comme dénominateur commun.

En pratique, on parle d'un démonstrateur à base de Garage et Restic.
Garage permet un stockage redondant, tolérant aux fautes, matériel hétérogène, peu puissant avec un proto standard S3, compatible avec Restic.

## Proposition 2 : l'île aux chatons

On prend tous les CHATONS et on fait un cluster Garage.
Point de friction : Gouvernance de l'îlot, super-pouvoirs individuels, volume de stockage hétérogène, traçabilité.

## Proposition 3 : l'archipel des CHATONS

Ensemble d'ilots Garage.
Les CHATONS qui ont envie montent leur ilot Garage ensemble, c'est à dire un cluster.
Des groupes de 3, 4 ou 5 CHATONS par exemple.
Comme ça on réduit le risque en cas de compromission,

Avantage aussi : on créer du lien entre CHATONS.

# Les doutes

Borg va bientot sortir une version 2.0 mais ils ne pensent pas
non plus implémenter S3. C'est embêtant car Borg est 
un logiciel de sauvegarde très populaire.

Question sur le fait que S3 pourrait etre un protocole qui ne soit pas libre ?
Comment on fait si Amazon décider de le changer de manière unilatérale ?
S3 est un protocole implémenté par une miriade d'outils, et Amazon n'a plus le monopole non plus dessus, plein de presta.

Consommation de ressources ?
NFS prend 34Mo de RAM en peek seulement.
Garage prendra plus. Actuellement 2Go en peek.
Travail en cours sur les perfs.
Possible d'utiliser sqlite+lmdb en place de LMDB qui pourrait réduire la conso de RAM.
Quentin Deuxfleurs prense que 500Mo peek

Est-ce qu'on serait obligé de créer 2 ilots minimum ?
Oui/Non ? On peut stocker sur son propre ilot sur ses sauvegardes,
mais problème organisationnel.
L'idée c'est de stocker ses données à un endroit que l'on ne gère pas du tout,
comme ça si le PC d'un-e admin est compromis, on perd pas les données.

On peut participer à deux ilots ?
Oui mais si on se fait corrompre, on corrompt 2 ilots, donc pas stratégique.
En effet chaque noeud peut compromettre l'ilot en entier.

Est-ce qu'on ne peut pas réduire les droits ?
C'est compliqué de designer un truc correct.
On préfère une mesure organisationnel à un truc bancale.

Est ce qu'il serait préconisé une sauvegarde locale ?
Sauvegarde distante longue à restaurer.
Dépend du cas d'usage et des propriétés qu'on veut avoir, c'est un compromis à réfléchir.
Picasoft non, mais si tu veux des garanties de service, oui il le faut !

# But du projet

Valider qu'organisationellement ça marche.
Faire des choses ensemble 
