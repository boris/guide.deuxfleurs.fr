---
title: Primevère 2023
weight: 100
draft: false
date: 2023-03-02
extra:
  parent: formations/conf.md
---
Le salon Primevère se tient une fois par an à Lyon à Eurexpo, c'est le salon de l'alter-écologie et on trouve donc tous les acteurs de ce milieu. Historiquement, il y a une proximité avec le monde du libre, d'où un “village du libre”. Le CHATONS Hadoly y est historiquement présent, mais on y trouve aussi l'ALDIL, Wikipedia, OpenstreetMap et bien d'autres personnes.

J'y étais en 2023 sur le stand de Hadoly, qui faisait aussi office de stand CHATONS. J'ai proposé un atelier “table rase numérique” et des petites brochures pour “la sobriété numérique”. Vous pouvez les télécharger ces ressources au format PDF :

- [Jeu “table rase numérique”](/img/default.pdf)
- [Brochure "anti-pub"](/img/pub.pdf)
- [Brochure "matériel durable"](/img/mat.pdf)
- [Brochure "obsolescence logiciel"](/img/obso.pdf)

Les fichiers sources éditables [sont dans mon dépôt git](https://git.deuxfleurs.fr/quentin/memo.git)
