---
title: "Guichet"
description: ""
date: 2021-11-09T12:39:27.819Z
dateCreated: 2021-11-09T12:39:25.808Z
weight: 20
extra:
  parent: 'infrastructures/logiciels.md'
---

# Guichet

Guichet est une interface de gestion utilisateur LDAP pour Bottin.
Il vise notamment à permettre aux utilisateurs de modifier leurs identifiants ainsi que leurs données personnelles.

## Statut du développement

Guichet est actuellement utilisé en production pour Deuxfleurs. Il est cependant toujours en développement actif.
Le code est ici : <https://git.deuxfleurs.fr/Deuxfleurs/guichet>
