---
title: "Serveurs"
description: "Serveurs"
weight: 40
sort_by: "weight"
extra:
  parent: 'infrastructures/_index.md'
---

# Rôles

Nous avons identifié 4 rôles pour nos serveurs, en fonction de la criticité des charges de travail
et des données qu'ils auront à gérer.

[Production](@/infrastructures/production.md) - Les serveurs de productions sont ceux qui font tourner les services accédés par les usager·es (eg. Plume, Matrix, Cryptpad).
Si ils sont inaccessibles, alors les services ne fonctionnent plus. Et si une personne malveillante y accède, elle peut avoir accès à des données
personnelles des usager·es. C'est donc le rôle le plus critique.

[Support](@/infrastructures/support.md) - Les serveurs de support servent pour les sauvegardes et la supervision des serveurs de production (eg. Grafana, Minio).
De par leur rôle, ils participent au bon fonctionnement de la production.
Ils n'ont pas de données personnelles brutes mais les métriques collectées peuvent refléter certains comportement des usager·es 
et les sauvegardes, bien qu'elles soient chiffrées, contiennent tout de même des données personnelles.

[Développement](@/infrastructures/developpement.md) - Les serveurs de développement hébergent les outils qui nous permettent de travailler sur le logiciel,
les configurations, les tickets, ou la compilation. Ils ne contiennent pas de données personnelles mais peuvent être utilisés pour
des attaques de chaîne d'approvisionnement (*supply chain attack*). À terme, ce rôle pourrait être fusionné avec la production.

[Expérimentation](@/infrastructures/xp.md) - Les serveurs d'expérimentation servent à tester les nouvelles configurations, les nouveaux logiciels,
et le nouveau matériel. Ils permettent aux opérateur·ices de se familiariser avec leurs modifications et de minimiser l'impact d'un changement sur les serveurs de production,
et donc sur la disponibilité des services. Ces machines ne contiennent pas de données personnelles et ne sont pas critiques, elles n'ont pas besoin de rester tout le temps allumées.
Il n'est pas nécessaire d'être opérateur·ice pour gérer une de ces machines.

# Zones

En fonction des propriétés voulues, nous pouvons être amenés à répartir les serveurs d'un rôle spécifique entre plusieurs lieux géographiques,
que nous appelons des zones.

Nous avons 3 zones pour la production :
  - Orsay (Neptune)
  - Lyon (Orion)
  - Bruxelles (Bespin)

Nous avons 2 zones pour le support :
  - Suresnes (Mercure)
  - Rennes (Jupiter)

Nous avons 1 zones pour le développement :
  - Bruxelles (Bespin)

Nous avons plusieurs zones pour l'expérimentation :
  - Orsay (Neptune)
  - Rennes (Jupiter)

