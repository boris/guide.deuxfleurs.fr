---
title: "Tricot"
description: ""
date: 2022-01-24T16:33:16.731Z
dateCreated: 2022-01-24T16:32:53.056Z
weight: 50
extra:
  parent: 'infrastructures/logiciels.md'
---

# Tricot

Tricot est un reverse-proxy HTTP et HTTPS qui s'auto-configure à partir de [Consul](https://www.consul.io), comme le faisait Traefik, qui posait de multiples problèmes et que nous avons décidé de remplacer. Tricot s'occupe automatiquement de récupérer des certificats au près de Let's Encrypt pour rendre tous les sites accessibles en TLS. Ceux-ci sont stockés dans Consul.

## Statut du développement

Tricot est actuellement utilisé en production pour Deuxfleurs. Il est cependant toujours en développement actif.
Le code de Tricot se trouve ici : <https://git.deuxfleurs.fr/Deuxfleurs/tricot>
