---
title: Développement logiciel
description: Logiciels
weight: 90
sort_by: weight
draft: false
date: 2024-01-24
extra:
  parent: infrastructures/_index.md
---

Cette section recense les logiciels développés par Deuxfleurs pour les besoins spécifiques de son infra.

# Principes de conception

Nou essayons de suivre plusieurs principes pour une conception qui correspond au besoin tout en ayant un ensemble de logiciels homogènes.

## Vie privée

Que ce soit à l'intérieur ou l'extérieur de l'association, des demandes pour d'avantage de garanties sur la vie privée ont été formulées.

### Propriétés recherchées

Quelques propriétés en vrac qu'on peut, ou ne pas, désirer :

### Messagerie instantanée

  - Je ne veux pas que le contenu de mes messages et des fichiers que je partage soient accessibles (eg. une photo que j'ai prise, mes réactions)
  - Je ne veux pas que les métadonnées autour de mes messages soient accessibles (eg. les salons de discussions auxquels je prends pars, l'horodatage des messages, les personnes avec qui j'échange)
  - Je ne veux pas que les métadonnées de communication soient accessibles (eg. quand je me connecte au service, depuis où, si j'intéragis sur le réseau, etc.), ces données permettent parfois d'inférer des métadonnées sur le protocole (autres personnes dans le salon de communication, horodatage, etc.)

### Courrier électronique (de l'email au mixnet)

  - Je ne veux pas que le contenu de mes emails et pièces jointes soit lisible (eg. le doc que j'ai joint)
  - Je ne veux pas que les métadonnées autour de mon message soient accessibles (eg. le destinataire, l'expéditeur, l'horodatage, le client email utilisé, le sujet du mail, le dossier dans lequel il est stocké)
  - Je ne veux pas que les métadonnées de communication soient accessibles (eg. quand je me connecte au service email, depuis où, quand j'intéragis sur le réseau), ces données permettent parfois d'inférer des métadonnées sur le protocole (destinataires, présence de pièce jointe, etc.)

### Synchronisation et collaboration sur des fichiers
  - Je ne veux pas que le contenu de mes fichiers soit accessible
  - Je ne veux pas que les métadonnées de mon fichier soient accessibles (eg. nom du fichier, dossier, format, taille, hash, etc.)
  - Je ne veux pas que les métadonnées de communication soient accessibles (eg. quand j'accède au document, depuis où, qui d'autre, combien de fois, etc.), ces données permettent parfois d'inférer des métadonnées sur le protocole (taille, collaborateurs, type, etc.)

## Attaquants

Quelques attaquants que l'on peut, ou ne pas, considérer :

  - Hébergeur de la machine (eg. branche un clavier et un écran sur l'ordi et récupère un accès admin)
  - Administrateur Système (eg. utilise ses accès privilégiés pour accéder volontairement ou non à du contenu privé)
  - Développeurs (eg. ajout d'une porte dérobée au moment de l'écriture du code)
  - Chaîne logistique (eg. ajout d'une porte dérobée au moment de déployer l'app sur les serveurs ou le terminal de l'utilisateur)
  - Opérateur Internet (eg. Orange)
  - Regroupement d'opérateurs internet (cf "Tor netflow")
  - Personne externe via internet (eg. hacker)
  - Personne externe physique (eg. voleur)
  - Regroupement d'acteurs (eg. opérateurs internet, externe physique ET internet)
  - Utilisateurs (eg. pas de chiffrement sur son téléphone)

## Un exemple de ce qu'on pourrait faire

Prenons l'exemple de la messagerie instantanée. Pour l'instant, on peut définir les types de réseaux suivants :

- centralisé, pas chiffré (Messenger)
- centralisé, chiffé de bout en bout (avec toute une gamme d'implems, la meilleure étant peut-être Signal)
- fédéré, pas chiffré (E-mail, IRC, XMPP sans omemo, Matrix sans E2EE)
- fédéré, chiffré (XMPP + Omemo, Matrix + E2EE)
- distribué, chiffré (Tox, Retroshare)
- distribué avec des mécanisme d'anonymat fort (Freenet, Mix networks, ...)

Seule la dernière catégorie s'adresse au cas d'un "global passive attacker". On peut imaginer d'abandonner l'idée d'avoir une protection très efficace contre ce dernier car ça serait très contraignant sur le design et l'utilisabilité, et les cas où on en aurait vraiment besoin sont des cas particuliers où on peut faire l'effort d'utiliser une solution adaptée.

Pour le "grand public", passer sur des solutions fédérés chiffrés c'est un très bon début et si on arrive déjà à passer des gens sur Matrix, c'est très bien. Ceci dit, il y a quand même une faille majeure dans ces systèmes, c'est l'existence de serveurs qui centralisent des quantités plus ou moins importantes de (méta)données : il suffit que n'importe lequel soit compromis pour que toutes ces données soient exfiltrées, global passive attacker ou non. En ce qui nous concerne chez Deuxfleurs, le danger est exacerbé car on veut faire de la réplication sur plusieurs noeuds chez plusieurs personnes, donc autant d'opportunités d'avoir une sécu pétée à un endroit ou un autre.

Une marche à franchir qui pourrait être désirable pour nous, ça serait donc de faire en sorte que les serveurs aient le moins de visibilité possible sur ce qui se passe dans le réseau. Dans un système "distribué chiffré" on n'a plus de serveurs donc le problème n'existe pas, le seul danger qui reste est la possibilité de compromettre le périphérique utilisateur directement (et on sait par ailleurs que c'est possible, mais c'est pas quelque chose sur lequel on peut agir nous). Par contre les systèmes 100% distribués ne sont pas utilisables efficacement sur mobile car ils pompent la batterie, d'où l'idée de vouloir faire une approche hybride où des serveurs pourraient jouer un rôle mais avec des protocoles prévus d'une manière à ce qu'ils ne voient passer et ne stockent qu'un minimum de données en clair.

Un autre problème, c'est que dans un modèle totalement distribué se pose la question de qui gère la résilience du service et des données ? Comment tu développes un logiciel distribué qui ne perd pas de données (bitcoin n'est pas une réponse valable) ?

On peut globalement diviser les approches en deux :

1. Définir des nouveaux protocoles client/serveur qui minimisent les métadonnées lisibles par les serveurs

2. Rester dans le cadre des protocoles standards (IMAP, WebDav, ...), et faire une implem où les serveurs stockent le plus possible de données chiffrées (y compris toutes les métadonnées), et ont le moins souvent possible accès à la donnée en clair.

La première approche est intéressante, mais elle implique de remplacer tout l'écosystème. Ça peut être un projet de recherche intéressant, mais on ne veut pas forcément en faire le projet prioritaire de Deuxfleurs.

Concernant la seconde approche, celle-ci semble beaucoup plus à notre portée :

- Par exemple dans une architecture de cluster à-la-Deuxfleurs, on pourrait différencier les lieux où on stocke les données en fonction du niveau de privacy : par exemple seuls les serveurs chez X seraient habilités à déchiffrer le contenu pour le servir sur des protocoles standards (S3, IMAP, Matrix), et les serveurs chez les autres seraient uniquement là pour stocker de la donnée entièrement chiffrée

- On peut imaginer que les clefs de chiffrement ne soient jamais stockées en clair nulle part : elles pourraient être stockées avec une passphrase qui serait le mot de passe de l'utilisateur (+ éventuellement un mécanisme de secret de shamir pour répartir les morceaux de clefs sur plusieurs machines), et c'est seulement au dernier moment (en RAM) qu'elles existent en version déchiffrée

- Enfin, on peut imaginer que Deuxfleurs fournisse à la place (ou en plus) du protocole standard un protocole entièrement chiffrée + un logiciel proxy que les utilisateurices pourraient installer sur leur machine pour avoir accès au protocole standard avec toutes leurs applications habituelles. Cette dernière option permettrait d'avoir le même niveau de sécurité que la solution consistant à définir de nouveaux protocoles, sans pour autant compromettre l'interopérabilité avec l'écosystème existant.

## Ressources

  - https://about.psyc.eu/Federation et https://about.psyc.eu/PSYC2
  - Définition d'un mixnet : https://www.youtube.com/watch?v=dQtk0NcTseg
