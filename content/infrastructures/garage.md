---
title: "Garage"
description: ""
date: 2021-11-09T12:42:59.273Z
dateCreated: 2021-11-09T12:42:57.245Z
weight: 40
extra:
  parent: 'infrastructures/logiciels.md'
---

# Garage

Garage est un utilitaire de stockage d'objets léger, distribué, et compatible avec l'interface Amazon S3. Il poursuit les objectifs suivants :

* être aussi autonome que possible
* être facile à installer
* demeurer robuste face aux pannes de réseau, à la latence du réseau, aux pannes de disques durs, et aux erreurs d'administrateurs système
* être simple
* être déployé sur de multiples centres de donnée

Il ne cherche pas à :

* fournir des performances exceptionnelles
* implémenter complètement l'API de S3
* implémenter des codes d'effacement (notre modèle consiste en dupliquer les données telles quelles sur plusieurs nœuds)

À l'heure actuelle, Garage est déployé sur notre grappe de serveurs (ce site même est hébergé sur Garage !), mais doit tout de même être considéré comme une démonstration technique.

Si vous voulez en savoir plus sur Garage, vous pouvez consulter notre [documentation](https://garagehq.deuxfleurs.fr/) :

* [Introduction rapide](https://garagehq.deuxfleurs.fr/documentation/quick-start/) : apprenez à interagir efficacement avec Garage
* [Travaux liés](https://garagehq.deuxfleurs.fr/documentation/design/related-work/) : pour comprendre pourquoi nous avons développé notre propre logiciel au lieu d'en choisir un existant
* [Technique interne](https://garagehq.deuxfleurs.fr/documentation/design/internals/) : une brève description des modèles de données utilisés dans Garage

Liens externes :

* [Dépôt](https://git.deuxfleurs.fr/Deuxfleurs/garage/) : Garage est un logiciel libre développé sur notre propre instance Gitea
* Article de médiation scientifique (PDF, français) : [Présentation de Deuxfleurs & Garage](https://www.societe-informatique-de-france.fr/wp-content/uploads/2023/11/1024_22_2023_171.html), novembre 2023  
* Présentation scientifique (PDF, anglais) : [Deuxfleurs & Garage presentation](https://git.deuxfleurs.fr/Deuxfleurs/garage/raw/branch/main/doc/talks/2023-01-18-tocatta/talk.pdf), janvier 2023  
* Présentation militante (vidéo & PDF, français) : [De l'auto-hébergement à l'entre-hébergement : Garage, pour conserver ses données ensemble](@/formations/capitole-du-libre-2022.md), novembre 2022 
* [Liste des présentations sur Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage/src/branch/main/doc/talks)

