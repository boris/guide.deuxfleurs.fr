---
title: "Développement"
description: "Développement"
weight: 30
extra:
  parent: 'infrastructures/machines.md'
---

Les serveurs de développement hébergent les outils qui nous permettent de travailler sur le logiciel,
les configurations, les tickets, ou la compilation. Ils ne contiennent pas de données personnelles mais peuvent être utilisés pour
des attaques de chaine d'approvisionnement (*supply chain attack*). 

# Bruxelles (Bespin)

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| Forge | VM | x1 | 16 cœurs, 8Go RAM, 25Go + 25Go + 50Go| `absinthe` |

# Autres runners Drone

## Rennes (Jupiter)

(information à rajouter)
