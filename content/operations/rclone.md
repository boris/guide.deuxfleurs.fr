---
title: "rclone"
description: "rclone"
weight: 20
sort_by: "weight"
extra:
  parent: 'operations/sauvegardes.md'
---

Script de backup brut, on planifie une approche plus élégante à l'avenir :

```
#!/bin/bash

cd $(dirname $0)

if [ "$(hostname)" != "io" ]; then
	echo "Please run this script on io"
	exit 1
fi

if [ ! -d "buckets" ]; then
	btrfs subvolume create $(pwd)/buckets
fi


AK=$1
SK=$2

function gctl {
	docker exec garage /garage $@
}

gctl status
BUCKETS=$(gctl bucket list | tail -n +2 | cut -d " " -f 3 | cut -d "," -f 1)

for BUCKET in $BUCKETS; do
	case $BUCKET in
		*backup*)
			echo "Skipping $BUCKET (not doing backup of backup)"
			;;
		*cache*)
			echo "Skipping $BUCKET (not doing backup of cache)"
			;;
		*)
			echo "Backing up $BUCKET"

			if [ ! -d $(pwd)/buckets/$BUCKET ]; then
				mkdir $(pwd)/buckets/$BUCKET
			fi

			gctl bucket allow --key $AK --read $BUCKET
			rclone sync --s3-endpoint http://localhost:3900 \
				--s3-access-key-id $AK \
				--s3-secret-access-key $SK \
				--s3-region garage \
				--s3-force-path-style \
				--transfers 32 \
				--fast-list \
				--stats-one-line \
				--stats 10s \
				--stats-log-level NOTICE \
		       		:s3:$BUCKET $(pwd)/buckets/$BUCKET
			;;
	esac
done

# Remove duplicates
#duperemove -dAr $(pwd)/buckets

if [ ! -d "$(pwd)/snapshots" ]; then
	mkdir snapshots
fi

SNAPSHOT=$(pwd)/snapshots/buckets-$(date +%F)
echo "Making snapshot: $SNAPSHOT"
btrfs subvolume snapshot $(pwd)/buckets $SNAPSHOT
```
