---
title: "Guide de création d'un nœud"
description: "Guide de création d'un nœud"
date: 2022-08-23
dateCreated: 2022-08-23
weight: 11
extra:
  parent: 'operations/noeud.md'
---

# Guide d'initialisation de nœud Deuxfleurs
Ce guide explique comment initialiser un nœud pour l'infrastructure de Deuxfleurs. Nous partons de zéro, c'est-à-dire avec une machine que nous venons de récupérer, avec une mémoire vide, et que nous venons de brancher. À titre d'exemple, nous illustrerons de temps en temps les opérations avec une de nos machines (un Thinkcentre de Lenovo).

## Configuration de l'UEFI
Configurons d'abord quelques paramètres dans l'UEFI de la machine. Démarrez-là et appuyez sur la touche pour accéder à ce menu. Chez nous, il s'agit de F1. Si le PXE est activé, désactivons-le : un attaquant présent sur le réseau local pourrait faire démarrer une machine sur une installation malveillante. Vérifions que les *C-States* sont pris en charge, cela permet une meilleure gestion de l'énergie. Configurons également la machine pour qu'elle démarre après avoir subi une coupure électrique, cela se révèlera pratique lorsqu'il y en aura une. Si l'option est là, autorisons le démarrage sans clavier branché, pour ne pas être embêté lorsque nous démarrons une machine pour SSH dessus. Enfin, dans le cadre de l'infrastructure Deuxfleurs, nous supporterons uniquement l'UEFI, nous pouvons donc désactiver les options de compatibilité BIOS.

## Installation de NixOS

> Aucun écran ou clavier n'est disponible pour l'ordinateur cible ? NixOS peut être installé en SSH, une page y est dédiée : [Installer NixOS sans écran](@/operations/SSH_sans_écran.md).

Pour installer NixOS, nous aurons besoin d'une clé USB avec une image amorçable (*live*) de NixOS dessus. Nous pouvons télécharger la distribution linux en 64 bits et en version minimale (sans interface graphique et avec moins d'utilitaires) sur le [site officiel](https://nixos.org/download.html). Pour écrire l'image sur le support USB, on peut faire `dd if=chemin-vers-le-fichier-iso of=/dev/sdX status=progress; sync`, en remplaçant `sdX` par le fichier périphérique correspondant à la clé, trouvé avec `lsblk` par exemple.
Alternativement, cela peut être l'occasion de créer une clé USB formatée avec [Ventoy](https://ventoy.net), un utilitaire très pratique
qui permet d'avoir plusieurs images ISO bootables depuis une même clef USB (utile si vous n'arrêtez pas de reformater vos clefs pour passer d'une distribution à une autre!)

Branchons la clé USB et démarrons dessus. Chez nous, c'est possible grâce à un menu accessible via la touche F12. Lançons NixOS sans option particulière. Accordons-nous tous les droits et configurons un clavier habituel. On peut également vérifier la connexion internet :
```
$ sudo su
# loadkeys fr-latin9
# ping deuxfleurs.fr
```
Nous pouvons faire `lsblk` pour examiner les disques de la machine. Chez nous, nous avons simplement un disque dur complètement vide de 500Go associé à `/dev/sda`. Nous allons formater le disque avec `cgdisk` :
```
# cgdisk /dev/sda
```
Nous créons d'abord, avec l'option `New`, une partition qui commence au début, fait 512Mo, avec un code hexadécimal `EF00`, et que nous appelerons «EFI» : c'est le secteur de démarrage.
Puis nous créons à la suite une partition de 8Go, avec un code hexadécimal `8200`, nommée «swap» : c'est l'espace d'échange.
Enfin sur tout le reste, nous créons une partition avec un code hexadécimal `8300`, que nous appelerons par exemple «root» : c'est la racine du système linux.
Pour appliquer les changements, nous utilisons l'option `Write`. Nous pouvons ensuite quitter avec `Quit`, et éventuellement vérifier le résultat avec `lsblk`.

Finalisons les partitions. Dans notre cas, nous devons créer les systèmes avec :
```
# mkfs.fat -F 32 /dev/sda1
# mkswap /dev/sda2
# mkfs.xfs /dev/sda3
```
Nous utilisons ici xfs car nous sommes équipés d'un disque rotatif.

Montons les partitions fraîchement créées.
```
# mount /dev/sda3 /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
# swapon /dev/sda2
```

> (06/2023) Alors qu'ils avaient directement monté un second disque sur `/mnt/mnt/storage`, Adrien & Lauric on vu échouer `nixos-install` avec l'erreur suivante : 
> 
> `rmdir: Failed to remove '/mnt': Directory not empty`. 
> 
> Si vous avez le même problème, essayez de recommencer l'installation avec uniquement le disque contenant le système (et configurez les autres disques plus tard).

Le résultat est vérifiable avec `df -h`. À ce stade, nous pouvons générer la configuration de NixOS dans cette arborescence, et l'éditer par exemple avec `nano` :
```
# nixos-generate-config --root /mnt
# nano /mnt/etc/nixos/configuration.nix
```
Ce fichier décrit la configuration du système de manière générale. NixOS le versionne, et à l'utilisation, chaque modification génère une nouvelle «génération». En cas d'erreur par exemple, nous pourrons revenir facilement à une génération précédente. Ainsi nous décrivons ici la première génération de notre système à venir. Nous n'allons en réalité modifier que quelques choses par rapport à la configuration par défaut. Décommentons et définissons le nom d'hôte et le fuseau horaire :
```
networking.hostName = "nomDeLaMachine";
```
```
time.timeZone = "Europe/Paris";
```
Pour les propriétés d'internationalisation, nous pouvons par exemple définir ceci :
```
i18n.defaultLocale = "fr_FR.UTF-8";
console = {
  font = "Lat2-Terminus16";
  keyMap = "fr-latin9";
};
```
Attention en tout cas à ne pas définir en même temps `keyMap` et `useXkbConfig`, ces deux options peuvent se contredire. Pour l'utilisateur et les paquets du système, nous pouvons par exemple partir sur :
```
users.users.nomUtilisateur = {
  isNormalUser = true;
  extraGroups = [ "wheel" ];
};
```
```
environment.systemPackages = with pkgs; [
  vim
  git
  wget
  emacs
];
```
L'installation de git est nécessaire pour rejoindre un cluster Deuxfleurs.

Enfin activons le serveur SSH en décommentant :
```
services.openssh.enable = true;
```
Nous pouvons enregistrer et fermer le fichier, puis lancer l'installation avec :
```
# nixos-install
```
Au bout d'un certain temps, le processus va nous demander le mot de passe pour le compte root. Une fois donné, l'installation devrait être terminée. Nous pouvons redémarrer sur la machine, et nous connecter en tant que root. Définissons le mot de passe de l'utilisateur spécifié dans la configuration auparavant (nomUtilisateur) avec :
```
# passwd nomUtilisateur
```
Nous pouvons si nous le voulons nous déconnecter avec `exit` et tester la connexion sur nomUtilisateur en local ou en SSH.
