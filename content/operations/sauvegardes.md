---
title: "Sauvegardes"
description: "Sauvegardes"
weight: 50
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

# Données sauvegardées


[restic](@/operations/restic.md) - Nous utilisons restic pour sauvegarder les logiciels 
qui utilisent le système de fichier (Cryptpad, Dovecot, et Plume) ainsi que Consul.
À terme, nous aimerions être en mesure de tout pouvoir stocker directement sur Garage
et rendre obsolète ce mode de sauvegarde.

[pg\_basebackup](@/operations/pg_basebackup.md) - Nous utilisons cet outils pour sauvegarder l'ensemble
des tables gérées par notre base de données SQL sans impacter trop les performances.
Le tout est réalisé par un script python qui chiffre avec [age](https://github.com/FiloSottile/age) et envoie le backup via S3.
À terme, nous aimerions utiliser [wal-g](https://github.com/wal-g/wal-g) à la place.

[rclone](@/operations/rclone.md) - Combiné avec btrfs, nous copions sur un système de fichier à plat
le contenu de notre cluster afin de faire face en cas de corruption.
À terme, nous aimerions remplacer cet outil par quelque chose de similaire à [s3s3mirror](https://github.com/cobbzilla/s3s3mirror).

# Localisation des sauvegardes

[Suresnes](@/infrastructures/support.md#suresnes-mercure) - À Suresnes, nous avons une instance Minio
dédiée aux sauvegardes de données. Elle reçoit les sauvegardes du système de fichier, de consul et de Postgres.

[Rennes 2](@/infrastructures/support.md#rennes-jupiter) - À Rennes, nous avons un simple serveur Debian
avec une partition en BTRFS. Il se charge de sauvegarder toutes les nuits le contenu de notre instance de production de Garage.
À terme il est possible qu'on décide de rationaliser nos sauvegardes et de choisir 
de sauvegarder S3.

# Durée de rétention et fréquence

Les sauvegardes doivent être configurées avec les options suivantes :

**Fréquence** - 1 fois par jour (toutes les nuits)   
**Durée de rétention** - 1 an  
**Politique de conservation des instantanés** - 1 instantané par jour pendant 1 mois, 1 instantané par semaine pendant 3 mois, 1 instantané par mois pendant 1 an

**Exceptions**  
Les sauvegardes de Postgres sont faites une fois par semaine seulement pour le moment  
Le nombre d'instantané est de 1 par jour pendant 1 an pour Garage
