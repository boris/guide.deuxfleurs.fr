---
title: "Configuration réseau OpenWrt"
description: "Configuration réseau OpenWrt"
date: 2024-05-30
dateCreated: 2024-05-30
weight: 20
extra:
  parent: 'operations/site.md'
---

# Pourquoi OpenWrt

Pour installer un site géographique, il y a deux possibilités pour gérer le réseau :

- utiliser la box fournie par le FAI
- utiliser son propre routeur

Certaines box de FAI sont limitées : elles ne permettent parfois pas de configurer le pare-feu IPv6, ou bien elles ont un support d'UPnP pas assez fiable pour les besoins de Deuxfleurs.

Dans ce cas, utiliser son propre routeur et y installer [OpenWrt](https://openwrt.org/) est un choix naturel : c'est un projet en logiciel libre, avec une grande communauté et beaucoup de modèles matériel supportés.
Par ailleurs, OpenWrt est très modulaire, on peut installer des paquets additionnels pour rajouter des fonctionnalités comme UPnP.

Attention, installer et configurer OpenWrt demande d'être à l'aise avec SSH, vim, et d'avoir quelques connaissances en réseau.

# Choisir un matériel supporté par OpenWrt

Voir [le site d'OpenWrt](https://openwrt.org/supported_devices).

# Installer OpenWrt

Voir [la documentation OpenWrt](https://openwrt.org/docs/guide-quick-start/start) et un [guide générique](https://openwrt.org/docs/guide-user/installation/generic.flashing).

# Configurer OpenWrt pour un site géographique Deuxfleurs

## Configuration de base

- installer OpenWrt
- se connecter en SSH sur le routeur
- définir un mot de passe root avec `passwd`, le renseigner dans le [dépôt des secrets](@/operations/pass.md)
- désactiver les IPv6 ULA : dans `/etc/config/network`, supprimer le bloc qui ressemble à ça :

```
config globals 'globals'
    option ula_prefix 'fda0:8093:6a4c::/48'
```

- autoriser le trafic IPv6 en entrée.  Attention, la configuration ci-dessous autorise absolument tout le trafic IPv6 pour toutes les machines du réseau local.  Vous pouvez être plus fin si nécessaire.
  Dans `/etc/config/firewall`, rajouter le bloc suivant :

```
config forwarding
	option src		wan
	option dest		lan
	option family		ipv6
```

- définir un nom d'hôte pour le routeur, par exemple `gw-<nom du site>`. Ça se passe dans `/etc/config/system`
- rebooter le routeur pour être sûr d'appliquer tous les changements

## Configuration UPnP

C'est le plus gros morceau.

- installer le serveur UPnP (ici pour un OpenWrt récent qui utilise nftables et non iptables) :

```bash
opkg update
opkg install miniupnpd-nftables
```

- configurer le serveur UPnP, ça se passe dans `/etc/config/upnpd` :
  - mettre `option enabled` à `1`
  - mettre `option enable_natpmp` à `0` (pas besoin pour Deuxfleurs)
  - mettre `option log_output` à `1` (pour faciliter le debug)
  - dans le premier bloc `perm_rule`, mettre `ext_ports` et `int_ports` à `'0-65535'` (autoriser à mapper tous les ports)
  - dans ce meme bloc, mettre la plage d'adresse IP des machines Deuxfleurs dans `int_addr`, par exemple `192.168.5.0/24` (pour éviter que d'autres machines n'utilisent UPnP)

- redémarrer le service UPnP : `/etc/init.d/miniupnpd restart`

Il est également nécessaire de changer le port utilisé par LuCI (l'interface web d'OpenWrt), sinon cela empechera de mapper les ports 80 et 443 vers Tricot.
Il faut veiller à choisir un port qui n'est utilisé par aucun service de Deuxfleurs.  Ici, on choisit 65080 en HTTP et 65443 en HTTPS.

- ouvrir `/etc/config/uhttpd`
- sur les lignes `listen_http`, changer `80` par `65080`
- sur les lignes `listen_https`, changer `443` par `65443`
- redémarrer le service : `/etc/init.d/uhttpd restart`

Une fois cette configuration effectuée, pour accéder à l'interface web du routeur, il faut aller sur `http://192.168.X.Y:65080`.

## Debug UPnP

Pour cela, il faut d'abord avoir un diplonat lancé par Nomad sur une machine du site.

Pour voir si le serveur UPnP reçoit bien les requetes de diplonat, afficher les logs OpenWrt avec `logread -f`.
