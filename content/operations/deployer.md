---
title: "Déployer du logiciel"
description: "Déploiement du logiciel"
sort_by: "weight"
date: 2022-12-22
weight: 30
extra:
  parent: 'operations/_index.md'
---


# Empaqueter

Packager avec nix un conteneur Docker, le publier

# Secrets

Créer les secrets avec `secretmgr`

# Service

Créer un service Nomad

Voir les différentes déclarations :
  - diplonat
  - tricot

# Sauvegardes

Voir la section appropriée

# Surveillance

Voir la section appropriée

