---
title: "Sources"
description: "Sources"
weight: 120
extra:
  parent: 'operations/_index.md'
---

## Accéder aux sources des logiciels utilisés

Deuxfleurs utilise exclusivement des logiciels libres dans le cadre de son activité. Vous pouvez retrouver ici la liste des sources des logiciels utilisés.

- [Debian](https://packages.debian.org/)
- [NixOS](https://github.com/NixOS)
- [Nomad](https://github.com/hashicorp/nomad)
- [Consul](https://github.com/hashicorp/consul/)
- [Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage)
- [Tricot](https://git.deuxfleurs.fr/Deuxfleurs/tricot)
- [Bottin](https://git.deuxfleurs.fr/Deuxfleurs/bottin/)
- [Guichet](https://git.deuxfleurs.fr/Deuxfleurs/guichet/)
- [Diplonat](https://git.deuxfleurs.fr/Deuxfleurs/diplonat/)
- [Stolon](https://github.com/sorintlab/stolon)
- [Docker](https://github.com/moby/moby)
- [Ansible](https://github.com/ansible/ansible)
- [Drone](https://github.com/harness/drone)
- [Restic](https://github.com/restic/restic)
- [Cryptpad](https://github.com/xwiki-labs/cryptpad)
- [Sogo](https://github.com/inverse-inc/sogo)
- [alps](https://deuxfleurs.fr/git.deuxfleurs.fr/Deuxfleurs/alps)
- [Synapse](https://github.com/matrix-org/synapse)
- [Element](https://github.com/vector-im/element-web)
- [Jitsi](https://github.com/jitsi)
- [Plume](https://github.com/Plume-org/Plume)
- [age](https://github.com/FiloSottile/age)
- [Minio](https://github.com/minio/minio)
- [Proxmox](https://git.proxmox.com/)
- [Grafana](https://github.com/grafana/grafana)
- [Prometheus](https://github.com/prometheus/prometheus)
- [Infra (legacy)](https://git.deuxfleurs.fr/Deuxfleurs/infrastructure)
- [Infra (next)](https://deuxfleurs.fr/git.deuxfleurs.fr/Deuxfleurs/nixcfg)

