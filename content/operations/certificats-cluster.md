---
title: "MàJ certificats du cluster"
description: "Mise à jour des certificats du cluster"
date: 2024-08-17
dateCreated: 2024-08-17
weight: 12
extra:
  parent: 'operations/maintien_en_condition.md'
---

Comment mettre à jour les certificats du cluster ?

Voir la [feuille de route de l'atelier 2024](https://pad.deuxfleurs.fr/code/#/2/code/view/mYz4-9xVfeuCUxkYbqB2vuT7gkGbmNPa-v+jRLhP+50/). 
