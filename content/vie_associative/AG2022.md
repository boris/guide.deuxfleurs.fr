---
title: "AG 2022"
description: "Troisième Assemblée Générale"
weight: 30
extra:
  parent: 'vie_associative/ag.md'
---


Le dimanche 27 février 2022, se tenait sur [Jitsi](https://jitsi.deuxfleurs.fr/) la troisième Assemblée Générale (AG) de Deuxfleurs : nous fêtions nos deux ans d'existence !

La retransmission de l'AG sera bientôt disponible en ligne. [Notre support de présentation est d'ores et déjà accessible.](https://p.adnab.me/slide/#/2/slide/view/ag3WVp8g4RrwRK5QgnLR2MyacoX4XBPnnRiJ+mX9d1I/embed/present/)

# En deux mots

* 11 membres étaient présents : Adrien, Alain, Alex, Axelle, Éric, Jill, Louison, Maximilien, Quentin, Trinity & Vincent.
* Nous avons mis à jour l'article 8 des statuts : nous élirons désormais le conseil d'administration (CA) au [jugement majoritaire](http://fr.wikipedia.org/wiki/Jugement_majoritaire), plutôt qu'au [scrutin de Condorcet randomisé](http://fr.wikipedia.org/wiki/Scrutin_de_Condorcet_randomis%C3%A9).
* Le bureau d'administration 2022 est composé d'Adrien, Maximilien & Vincent (le bureau sortant, reconduit), et d'Alain, nouvellement élu.


Nous nous réjouissons d'accueillir un membre non-fondateur dans notre organe de décision. Alain dispose d'une grande expérience en « entrepreneuriat », ayant manœuvré nombre d'associations et entreprises au long de sa vie. Nul doute que sa précense à nos côtés nous fera grand bien !


# Présentation de l'association 

Nous avons rappelé la *raison d'être* de Deuxfleurs : la **démocratisation** du web. Ce terme porte les valeurs d'inclusivité, de convivialité et d'humanisme qui nous sont chères. La frugalité et l'autonomie sont aussi importantes à nos yeux.

Deuxfleurs porte 3 actions principales : la **conception** de nos outils numériques, la **construction** de notre infrastructure web (utilisant notamment nos outils), et l'**essaimage** de nos connaissances et idées.

# Changement du mode de scrutin

> ## Modification de l'article 8 de nos [statuts](@/vie_associative/statuts.md)
> 
> [...] Elle [L'Assemblée Générale] pourvoit, au scrutin secret, à la nomination ou au renouvellement des membres du conseil d'administration via un scrutin **au jugement majoritaire**. Elle fixe le montant de la cotisation annuelle. Les décisions de l'assemblée sont prises à la majorité des membres présents ou représentés. Chaque membre présent ne peut détenir plus d'une procuration.

Vote organisé via sondage Jitsi, unanimité pour (7 votes exprimés).

# Bilan moral 2021

En 2021, nous avons suivi 3 principaux objectifs :

1. ☂️ devenir résilient
2. 🍃 promouvoir des outils sobres
3. 🧠 avoir une approche critique

## 1. ☂️ devenir résilient

Les pannes sont la norme en hébergement informatique : alimentation, réseaux, matériel, logiciel, erreurs humaines... Nous en avons déjà fait les frais, et nous aimerions dormir la nuit. C'est pourquoi nous souhaitons construire un écosystème résilient. Nous visons deux propriétés :

1. disperser géographiquement nos données
2. assurer la disponibilité des services

Hélas, personne n'a jamais réalisé de géodistribution telle que nous l'entendons, si bien qu'aucun logiciel ne sait assurer de bonne qualité de service dans ces conditions. C'est pourquoi il nous revient de créer ce savoir-faire, et de développer des logiciels adaptés. 

Sur la voie de la résilience, **certains chantiers ont bien avancé**. Nous avons remplacé quelques services défectueux (GlusterFS → Garage, Traefik → Tricot). Nous faisons davantage de sauvegardes automatiques, nous visualisons mieux l'état de santé du cluster, et nous écrivons progressivement des guides & procédés d'administration.

**D'autres chantiers démarrent**, et principalement l'élimination du point critique de Rennes, où la majeure partie de nos services est hébergée. Pour ce faire, nous devons proposer un modèle de sécurité nous permettant d'interconnecter notre cluster sur Internet. Il nous faut par ailleurs nous préparer à accueillir d'autres hébergeureuses, en assurant notamment leur formation technique.

Rappelons nos outils activement développés : 

* [Garage](https://garagehq.deuxfleurs.fr), stockage objet distribué (on en reparle).
* [Diplonat](/Technique/Developpement/Diplonat), le diplomate des serveurs hébergés dans un réseau dynamique (comme un domicile).
* [Bottin](/Technique/Developpement/Bottin), notre annuaire LDAP.
* [Guichet](/Technique/Developpement/Guichet), l'interface web de gestion et d'administration du Bottin.
* [Tricot](/Technique/Developpement/Tricot), le petit dernier. C'est un serveur web/reverse proxy, remplaçant direct de Traefik.

### NGI POINTER subventionne le développement de Garage

Cette année, Deuxfleurs a reçu une subvention du fonds européen [NGI POINTER](https://www.ngi.eu/ngi-projects/ngi-pointer/), comme nous vous le racontions en septembre [sur notre blog](https://plume.deuxfleurs.fr/~/Deuxfleurs/NGI%20Pointer%20subventionne%20Deuxfleurs). Cette subvention de 193k€ nous permet principalement de salarier Quentin, Alex, et Jill, afin qu'iels travaillent au développement de Garage, d'octobre 2021 à septembre 2022. 

La subvention est subdivisée en cinq lots, chacun étant versé après réalisation d'un jalon par Deuxfleurs. À ce jour, nous avons réalisé 3 jalons sur 5. Notre impressionnant rythme de travail nous donne confiance en notre stabilité économique. Comme prévu dans le budget, nous avons commencé à investir une partie de la subvention dans la communication visuelle (voir [le nouveau site de Garage](https://garagehq.deuxfleurs.fr)), et dans des achats matériels (voir [nos nouveaux vieux ordinateurs](https://plume.deuxfleurs.fr/~/Deuxfleurs/Nos%20nouveaux%20vieux%20ordinateurs)).

**Temps forts** : 

- Dès septembre, Garage a été repéré par SUNET (service internet Suédois pour la recherche).
- En février 2021, nous avons simultanément sorti la première béta publique de Garage (v0.6.0), et [présenté Garage au FOSDEM](https://plume.deuxfleurs.fr/~/Deuxfleurs/Garage%20sera%20%C3%A0%20la%20conf%C3%A9rence%20du%20logiciel%20libre%20%22FOSDEM%202022%22). 
- Le même mois, nous communiquions publiquement sur Garage dans [Hacker News](https://news.ycombinator.com/item?id=30256753), [Reddit](https://www.reddit.com/r/selfhosted/comments/snh0ed/introducing_garage_our_selfhosted_distributed/) ([deux fois](https://www.reddit.com/r/rust/comments/snnlf2/garage_an_opensource_distributed_storage_service/)), et sur Lobster. Les retours nombreux et globalement très positifs furent très encourageants.

## 2. 🍃 promouvoir des outils sobres

Tout d'abord, nous sommes très fiers d'avoir mis la main sur 35 [nouveaux vieux ordinateurs](https://plume.deuxfleurs.fr/~/Deuxfleurs/Nos%20nouveaux%20vieux%20ordinateurs), dont se séparait l'INSEE de Bordeaux, pour environ 40€/pièce ! De quoi quoi construire de beaux clusters de seconde main.

Deuxfleurs a sa propre vision des outils numériques qu'elle souhaite promouvoir et supporter (sites statiques, par exemple). Pour ceux-là, l'association souhaite contribuer à l'accompagnement de ses utilisateur⋅ices (le [guide d'usage de Matrix écrit par Adrien](https://guide.zinz.dev), par exemple).

Nous promouvons par exemple les **sites statiques** : en comparaison des réseaux sociaux et des CMS traditionnels (Wordpress, Joomla, Drupal...), les sites statiques sont une solution de publication sur Internet qu'on trouve plus rapide, économe et simple. (Pour plus de détails, il fallait y être, ou il faudra attendre qu'on vous en reparle !)

Les **e-mails** sont une autre technologie que nous défendons, bien que parfois considérée obsolète par les professionnels. 



## 🧠 avoir une approche critique

Notre quête de pensée critique est d'abord tournée vers nous-même : la réflexivité. Cette année, l'association [Anciela](https://www.anciela.info/) nous a aidés à réfléchir à nos pratiques.

D'autre part, nous sommes friands d'avis contradictoires, et souhaitons donc confronter nos idées au public. Pour ce faire, nous publions régulièrement sur notre blog, [Plume](https://plume.deuxfleurs.fr). Nous sommes aussi engagés dans la candidature d'Esther à [la résidence artistique de la Villa Libertine](https://villa-albertine.org/fr/professionals/appel-candidatures-de-la-villa-albertine-saison-2) - ce qui serait une occasion de confronter nos idées outre-Atlantique.

# Bilan financier 2021

En 2021 :

* Revenus : **+72 660€**

	* Cotisations 2021 (+2022) : 190€
  * Dons adhérent⋅es : 95€
  * Subvention NGI POINTER pour Garage : 72 375€

* Dépenses : **-21 586,46€**

	* Frais bancaires : -137,94€
  * Projet Garage : -21 448,52€

* Solde au 31 décembre 2021 : **+53 122,70€**
  
Depuis le début de l'année 2022 (à date du 26 février 2022) : 

* Revenus : **0 €**
* Dépenses : **-25 384,02€**

	* Cotisations 2022 : +70€
  * Dons adhérent⋅es : +95€
  * Projet Garage : -25 529,02€
  
* Solde au 26 février 2022 : **+27 758,68€**

Détail financier du projet NGI (à date du 26 février 2022) : 

<table border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;">
  <colgroup>
    <col/>
    <col/>
    <col/>
    <col/>
  </colgroup>
  <tr style="font-weight:bold;background-color:#bdbdbd;">
    <td style="text-align:left;">
      <p>Catégorie:Sous-catégorie
      </p>
    </td>
    <td style="text-align:right;">
      <p>Dépenses (€)
      </p>
    </td>
    <td style="text-align:right;">
      <p>Revenus (€)
      </p>
    </td>
    <td style="text-align:right;">
      <p>Total (€)
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Achats:Matériel:PC
      </p>
    </td>
    <td style="text-align:right;">
      <p>-1387
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-1387
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Cotisations sociales:AST
      </p>
    </td>
    <td style="text-align:right;">
      <p>-367
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-367
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Cotisations sociales:OPCO
      </p>
    </td>
    <td style="text-align:right;">
      <p>-606
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-606
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Cotisations sociales:URSSAF
      </p>
    </td>
    <td style="text-align:right;">
      <p>-14408
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-14408
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Frais
      </p>
    </td>
    <td style="text-align:right;">
      <p>-45
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-45
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Funding:NGI
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>72375
      </p>
    </td>
    <td style="text-align:right;">
      <p>72375
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Prestations:Webdesign
      </p>
    </td>
    <td style="text-align:right;">
      <p>-1000
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-1000
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Salariat:Comptable
      </p>
    </td>
    <td style="text-align:right;">
      <p>-972
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-972
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Salariat:Impôts
      </p>
    </td>
    <td style="text-align:right;">
      <p>-914
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-914
      </p>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">
      <p>Salariat:Salaire brut
      </p>
    </td>
    <td style="text-align:right;">
      <p>-25357
      </p>
    </td>
    <td style="text-align:right;">
      <p>0
      </p>
    </td>
    <td style="text-align:right;">
      <p>-25357
      </p>
    </td>
  </tr>
  <tr style="background-color:#fffaaa; font-weight:bold;">
    <td style="text-align:left;">
      <p>Total au 26 Février 2022
      </p>
    </td>
    <td style="text-align:right;">
      <p>-46977
      </p>
    </td>
    <td style="text-align:right;">
      <p>72375
      </p>
    </td>
    <td style="text-align:right;">
      <p>27316
      </p>
    </td>
  </tr>
</table>

# Perspectives 2022

Poursuivre les objectifs de l'an passé : 

1. ☂️ devenir résilient
2. 🍃 promouvoir des outils sobres
3. 🧠 avoir une approche critique

En adaptant notre approche : de l'exploration, à la **focalisation** vers les chantiers où nous pourrons au mieux employer nos forces.

## Des « cercles » pour nous focaliser

Pour focaliser notre action, nous souhaiterions créer des « cercles » (ou groupes de travail) au sein de l'association.

Créer un cercle ne demande que quelques étapes : 

1. 👤 désigner un·e référent·e (qui centralise la info, ce n'est pas un⋅e chef⋅fe)
2. 🗓️ définir des réunions récurrentes
3. 📣 créer des espaces de communication
4. 📌 se rendre visible

Quelques suggestions de cercles : 

* Usages : faciliter prise en main des outils
* Recherche & Développement : conception logiciels
* Infrastructure : matériel et services
* Financement : trouver de l'argent & des partenaires
* Essaimage : partager nos idées, en découvrir de nouvelles
* Loin du clavier : créer des occasions de se rencontrer en physique

Vos propositions sont les bienvenues !

## ⛺ Se voir loin du clavier ?

On aimerait créer des occasions de nous rencontrer. Il suffit de cette association qui ne vit que sur Internet ! Que ça soit pour un week-end sans feuille de route, ou pour un camp d'une semaine dans un châlet alpin avec un programme défini et musclé. 

D'autres le font : on pense au [Camp CHATONS](https://video.colibris-outilslibres.org/videos/watch/e320c9f7-e1d4-4e47-bc92-eacb1280e282), et au [Camp Indie Hosters](https://indiehosters.net/blog/2021/09/17/c-est-la-rentree-chez-indiehosters.html).

Si vous êtes intéressé⋅es, ya plus qu'à créer un cercle !

## 🏁 La prochaine étape 

Mettons en place ces cercles ! On peut commencer par en discuter sur <a href="https://matrix.to/#/#forum:deuxfleurs.fr">deuxfleurs::forum</a>.

Individuellement, vous n'avez qu'à penser aux cercles que vous voudriez rejoindre, si vous souhaitez être référent⋅e, et à vos contraintes de planning pour les réunions récurrentes.

# Élection du bureau 2022

> [mieuxvoter.fr](https://mieuxvoter.fr/) est un site proposant de créer des scrutins au Jugement Majoritaire, qui envoie un e-mail personnalisé par votant, son « bulletin de vote ». **L'envoi de mails est prohibitivement long pour nous (supérieur à 1h pour certains destinataires).** Si vous comptez l'utiliser, prévoyez d'envoyer le scrutin la veille.
>
> L'an prochain : on crée notre propre solution de vote. --'
{.is-warning}

Candidats : Adrien, Alain, Maximilien & Vincent.

9 votes exprimés.

![ag2022_resultats_vote.png](/ag2022_resultats_vote.png)

Tous les candidats sont élus : le conseil d'administration 2022 est constitué de : Adrien, Alain, Maximilien & Vincent.

# Questions

* Quentin : Alors, ça vous branche le camp ?

	Adrien : Oui, mais j'ai toujours peur de mon emploi du temps. En tout cas, j'ai très envie de toutes et tous vous voir !
  
* Maximilien :  prévoir des réunions mensuelles récurrentes, dont on fixe la date en avance. Logistique plus simple que de demander les dispos de toute le monde à chaque réunion ponctuelle.

	Oui carrément.
  
* Vincent : Profiter de Pas Sages en Seine pour faire une rencontre Deuxfleurs.

---

Allez, emballé jusqu'à l'année prochaine !
Deuxfleuristement vôtre,
Adrien



